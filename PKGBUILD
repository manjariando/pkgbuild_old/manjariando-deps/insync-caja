# Maintainer: Mark Weiman <mark dot weiman at markzz dot com>
# Contributor: Zhengyu Xu <xzy3186@gmail.com>

pkgname=insync-caja
pkgver=3.8.1.50459
pkgrel=1
pkgdesc="Caja integration for insync"
url="https://www.insynchq.com/downloads"
license=('custom:insync')
options=('!strip' '!emptydirs')
arch=('x86_64')
makedepends=('imagemagick')
source=("http://cdn.insynchq.com/builds/linux/${pkgname}_${pkgver}_all.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/filemanager.png")
sha256sums=('d107f5506a52618d815bd3f403b4e95221e86986fc15e2f7b14b4b937886de84'
            'f35f2b4cb664e359fdab582d567a2873238dc71570025c26f148088d7cd6a53a'
            '3b19be9e841750dd69bc8bedb141dc74a9cac7ec6a885223cc5d1bf26af6d925')
noextract=("${pkgname}_${pkgver}_all.deb")

_insync_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=insync start
TryExec=insync
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_insync_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=("insync>=${pkgver%.*}" "python-caja")
    optdepends=("insync-emblem-icons: emblem icons for Insync")

    cd ${srcdir}
    ar x ${pkgname}_${pkgver}_all.deb
    tar xvf data.tar.gz
    cp -rp usr ${pkgdir}
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/filemanager.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
